import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, MenuItem, Button } from 'react-bootstrap';
class Menu extends Component{

    constructor(props){
        super(props);
        this.logout = this.logout.bind(this);
    }

    logout = () => {
        sessionStorage.setItem("usuario", "");
        window.location.href = "/login";
    }

    render(){
        return(
            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="/home"> Ekki </a>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <NavDropdown title="Transfers">
                            <MenuItem href="/makeTransfer"> Make a transfer </MenuItem>
                            <MenuItem href="/transfers"> View transfers </MenuItem>
                        </NavDropdown>
                    </Nav>
                    
                    <Nav>
                        <NavDropdown title="Credit Cards">
                            <MenuItem href="/saveCreditCard"> Create Credit Card </MenuItem>
                            <MenuItem href="/listCreditCards"> List Credit Cards </MenuItem>
                        </NavDropdown>
                    </Nav>
                
                
                    <Nav>
                        <NavDropdown title="Contacts">
                            <MenuItem href="/saveContact"> Create Contact </MenuItem>
                            <MenuItem href="/listContacts"> List Contacts </MenuItem>
                        </NavDropdown>
                    </Nav>

                    <Nav pullRight>
                        <MenuItem> 
                            <Button onClick={this.logout}> Logout </Button>
                        </MenuItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }

}

export default Menu;