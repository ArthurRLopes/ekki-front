import React, { Component } from 'react';
import { Alert, Button } from 'react-bootstrap';
import '../styles/transfer_alert.css';
import { Link } from 'react-router-dom';

class TransferAlert extends Component{

    constructor(props){
        super(props);
    }

    render(){
        const button = this.props.withButton ? <Link to='/saveCreditCard'><Button> Register </Button> </Link> : ''
        if (this.props.show){
            return(
                <div id="alert">
                    <Alert bsStyle="danger"> {this.props.errorMessage} {button} </Alert>
                </div>
            );
        } else {
            return null;
        }
    }

}

export default TransferAlert;