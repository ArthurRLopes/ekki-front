import React, { Component } from 'react';
import Menu from './Menu.jsx';
import Axios from 'axios';
import { Table } from 'react-bootstrap';

class Transfers extends Component{
    constructor(props){
        super(props);
        this.state = {
            transfers: []
        }
    }

    componentWillMount(){
        let _this = this;
        Axios.get('http://localhost:3001/transactions/' + sessionStorage.getItem('usuario'))
        .then(function (response){
            _this.setState({
                transfers: response.data.data
            })
        }).catch(function (error){
            console.log(error);
        })
    }

    render(){
        let transfers = this.state.transfers;
        
        return(
            <div>
                <Menu />
                <Table striped bordered condensed hover responsive>
                    <thead>
                        <tr>
                            <th> From </th>
                            <th> To </th>
                            <th> Amount </th>
                            <th> Date </th>
                        </tr>
                    </thead>
                    <tbody>
                        {transfers.map(
                            transfer => 
                                <tr>
                                    <td> {transfer.idaccountfrom} </td>
                                    <td> {transfer.idaccountto} </td>
                                    <td> {transfer.amount} </td>
                                    <td> {transfer.datetime} </td>
                                </tr>
                        )}
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default Transfers;