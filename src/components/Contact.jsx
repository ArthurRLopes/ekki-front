import React, { Component } from 'react';
import Menu from './Menu.jsx';
import { FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap';
import Axios from 'axios';

class Contact extends Component{

    constructor(props){
        super(props);
        this.state = {
            idAccount: '',
            name: '',
            phone: ''
        }
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        let _this = this;
        if (this.props.match.params.id){
            Axios.put('http://localhost:3001/contacts/' + this.props.match.params.id, {
                name: _this.state.name,
                phone: _this.state.phone,
                idAccount: sessionStorage.getItem("usuario")
            }).then(function(response){
                alert('Contact succesfully saved');
                window.location.reload();
            }).catch(function(error){
                console.log(error);
            });
        } else {
            Axios.post('http://localhost:3001/contacts/' + sessionStorage.getItem("usuario"), {
                name: _this.state.name,
                phone: _this.state.phone,
                idAccount: sessionStorage.getItem("usuario")
            })
            .then(function(response){
                alert('Contact succesfully saved');
                _this.props.history.push('/home');
            }).catch(function(error){
                console.log(error);
            });
        }
        event.preventDefault();
    }

    componentWillMount(){
        
        if (this.props.match.params.id){
            let id = this.props.match.params.id;
            let _this = this;

            Axios.get('http://localhost:3001/contacts/' + id)
            .then(function(response){
                let data = response.data.data[0];
                _this.setState({
                    name: data.name,
                    phone: data.phone,
                });
            })
        }
    }

    render(){
        return(
            <div>
                <Menu />
                <h1> Contact </h1>
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="name">
                        <ControlLabel> Name: </ControlLabel>
                        <FormControl type="text" onChange={this.handleChange} value={this.state.name}/>
                    </FormGroup>

                    <FormGroup controlId="phone">
                        <ControlLabel> Phone: </ControlLabel>
                        <FormControl onChange={this.handleChange} value={this.state.phone}/>
                    </FormGroup>
                    
                    <Button type="submit">
                        Save
                    </Button>
                </form>
            </div>
            
        );
    }

}

export default Contact;