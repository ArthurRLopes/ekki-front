import React, { Component } from 'react';
import Menu from './Menu.jsx';
import Axios from 'axios';
import { Table, Button, Glyphicon } from 'react-bootstrap';

class Contacts extends Component{
    constructor(props){
        super(props);
        this.state = {
            contacts: []
        }
        this.deleteRow = this.deleteRow.bind(this);
        this.editRow = this.editRow.bind(this);
    }

    componentWillMount(){
        let _this = this;
        Axios.get('http://localhost:3001/contacts/' + sessionStorage.getItem('usuario'))
        .then(function (response){
            _this.setState({
                contacts: response.data.data
            })
        }).catch(function (error){
            console.log(error);
        })
    }

    editRow = (contact) => {
        this.props.history.push('/saveContact/' + contact.id);
    }

    deleteRow = (contact) => {
        
        Axios.delete('http://localhost:3001/contacts/' + contact.id)
        .then(function(response){
            alert('Contact succesfully deleted');
        }).catch(function (error){
            console.log(error);
        })

        window.location.reload();

    }

    render(){
        let contacts = this.state.contacts;
        
        return(
            <div>
                <Menu />
                <Table striped bordered condensed hover responsive>
                    <thead>
                        <tr>
                            <th> Name </th>
                            <th> Phone </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                        {contacts.map(
                            contact => 
                                <tr>
                                    <td> {contact.name} </td>
                                    <td> {contact.phone} </td>
                                    <td> 
                                        <Button onClick={this.editRow.bind(this, contact)}> <Glyphicon glyph="edit" /> </Button>
                                        <Button onClick={this.deleteRow.bind(this, contact)}> <Glyphicon glyph="remove" /> </Button> 
                                    </td>
                                </tr>
                        )}
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default Contacts;