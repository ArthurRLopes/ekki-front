import React, { Component } from "react";
import Axios from "axios";
import Menu from './Menu.jsx';

class Home extends Component{

    constructor(props){
        super(props);
        this.state = {
            name: '',
            balance: '',
        }
    }

    componentDidMount(){
        console.log(sessionStorage.getItem("usuario"));
        let _this = this;
        Axios.get('http://localhost:3001/users/get/' + sessionStorage.getItem("usuario"))
        .then(function (response){
            _this.setState({
                name: response.data.data.name,
                balance: response.data.data.balance.toFixed(2)
            })
        }).catch(function (error){
            console.log(error);
        });
    }

    render(){
        return(
            <div>
                <Menu />
                <h1> Hello {this.state.name}, your current balance is ${this.state.balance} </h1>
            </div>
        );
    }

}

export default Home;