import React, { Component } from 'react';
import Menu from './Menu.jsx';
import { FormGroup, FormControl, ControlLabel, Button, HelpBlock } from 'react-bootstrap';
import Axios from 'axios';
import '../styles/creditcard.css'

class CreditCard extends Component{

    constructor(props){
        super(props);
        this.state = {
            number: '',
            securityCode: '',
            name: '',
            expiryDateMonth: '',
            expiryDateYear: '',
            id: ''
        }
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        let _this = this;
        if (this.props.match.params.id){
            Axios.put('http://localhost:3001/creditCards/' + this.props.match.params.id, {
                number: _this.state.number,
                securityCode: _this.state.securityCode,
                name: _this.state.name,
                expiryDateMonth: _this.state.expiryDateMonth,
                expiryDateYear: _this.state.expiryDateYear,
                idAccount: sessionStorage.getItem("usuario")
            }).then(function(response){
                alert('Credit Card succesfully saved');
                window.location.reload();
            }).catch(function(error){
                console.log(error);
            });
        } else {
            Axios.post('http://localhost:3001/creditCards/' + sessionStorage.getItem("usuario"), {
                number: _this.state.number,
                securityCode: _this.state.securityCode,
                name: _this.state.name,
                expiryDateMonth: _this.state.expiryDateMonth,
                expiryDateYear: _this.state.expiryDateYear,
                idAccount: sessionStorage.getItem("usuario")
            })
            .then(function(response){
                alert('Credit Card succesfully saved');
                _this.props.history.push('/home');
            }).catch(function(error){
                console.log(error);
            });
        }
        event.preventDefault();
    }

    componentWillMount(){
        
        if (this.props.match.params.id){
            let id = this.props.match.params.id;
            let _this = this;

            Axios.get('http://localhost:3001/creditCards/' + id)
            .then(function(response){
                let data = response.data.data[0];
                _this.setState({
                    number: data.number,
                    securityCode: data.securityCode,
                    name: data.name,
                    expiryDateMonth: data.expiryDateMonth,
                    expiryDateYear: data.expiryDateYear,
                });
            })
        }
    }

    validateNumber(){
        return this.state.number.length == 5 ? 'success' : 'error';
    }

    validateSecurityCode(){
        return this.state.securityCode.length == 3 ? 'success' : 'error';
    }

    validateExpiryDateMonth(){
        return this.state.expiryDateMonth.length == 2 ? 'success' : 'error';
    }

    validateExpiryDateYear(){
        return this.state.expiryDateYear.length == 2 ? 'success' : 'error';
    }

    render(){
        return(
            <div>
                <Menu />
                <h1> Credit Card </h1>
                <form onSubmit={this.handleSubmit}>
                    <FormGroup className="input-fields" controlId="number" validationState={this.validateNumber()}>
                        <ControlLabel> Number: </ControlLabel>
                        <FormControl type="text" onChange={this.handleChange} value={this.state.number}/>
                        <FormControl.Feedback />
                        <HelpBlock> 16 characters </HelpBlock>
                    </FormGroup>
                    
                    <FormGroup className="input-fields" controlId="securityCode" validationState={this.validateSecurityCode()}>
                        <ControlLabel> Security Code: </ControlLabel>
                        <FormControl maxLength="3" type="text" onChange={this.handleChange} value={this.state.securityCode}/>
                        <FormControl.Feedback />
                        <HelpBlock> 3 characters </HelpBlock>
                    </FormGroup>
                    <FormGroup className="input-fields" controlId="name">
                        <ControlLabel> Name: </ControlLabel>
                        <FormControl type="text" onChange={this.handleChange} value={this.state.name}/>
                    </FormGroup>
                    <FormGroup className="input-fields" controlId="expiryDateMonth" validationState={this.validateExpiryDateMonth()}>
                        <ControlLabel> Expiry Date Month: </ControlLabel>
                        <FormControl maxLength="2" type="text" onChange={this.handleChange} value={this.state.expiryDateMonth}/>
                        <FormControl.Feedback />
                        <HelpBlock> 2 characters </HelpBlock>
                    </FormGroup>
                    <FormGroup className="input-fields" controlId="expiryDateYear" validationState={this.validateExpiryDateYear()}>
                        <ControlLabel> Expiry Date Year: </ControlLabel>
                        <FormControl maxLength="2" type="text" onChange={this.handleChange} value={this.state.expiryDateYear}/>
                        <FormControl.Feedback />
                        <HelpBlock> 2 characters </HelpBlock>
                    </FormGroup>
                    <Button type="submit">
                        Save
                    </Button>
                </form>
            </div>
            
        );
    }

}

export default CreditCard;