import React, { Component } from 'react';
import { FormGroup, FormControl, ControlLabel, Button, Navbar } from 'react-bootstrap';
import Axios from 'axios';

class CreateAccount extends Component{

    constructor(props){
        super(props);
        this.state = {
            name: '',
            email: '',
            password: '',
            document: '',
            balance: ''
        }
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        let _this = this;
        
        Axios.post('http://localhost:3001/users/', {
            name: _this.state.name,
            email: _this.state.email,
            password: _this.state.password,
            document: _this.state.document,
            balance: _this.state.balance
        })
        .then(function(response){
            alert('Account succesfully saved');
            _this.props.history.push('/login');
        }).catch(function(error){
            console.log(error);
        });
        
        event.preventDefault();
    }

    render(){
        return(
            <div>
                <Navbar inverse collapseOnSelect>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <a href="/home"> Ekki </a>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                </Navbar>
                <h1> Create Account </h1>
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="name">
                        <ControlLabel> Name: </ControlLabel>
                        <FormControl type="text" onChange={this.handleChange} value={this.state.name}/>
                    </FormGroup>

                    <FormGroup controlId="email">
                        <ControlLabel> E-mail: </ControlLabel>
                        <FormControl onChange={this.handleChange} value={this.state.email}/>
                    </FormGroup>
                    
                    <FormGroup controlId="password">
                        <ControlLabel> Password: </ControlLabel>
                        <FormControl type="password" onChange={this.handleChange} value={this.state.password}/>
                    </FormGroup>

                    <FormGroup controlId="document">
                        <ControlLabel> Document: </ControlLabel>
                        <FormControl onChange={this.handleChange} value={this.state.document}/>
                    </FormGroup>

                    <FormGroup controlId="balance">
                        <ControlLabel> Balance: </ControlLabel>
                        <FormControl type="number" step=".01" onChange={this.handleChange} value={this.state.balance}/>
                    </FormGroup>

                    <Button type="submit">
                        Save
                    </Button>
                </form>
            </div>
            
        );
    }

}

export default CreateAccount;