import React, { Component } from 'react';
import Menu from './Menu.jsx';
import Axios from 'axios';
import { Table, Button, Glyphicon } from 'react-bootstrap';

class CreditCards extends Component{
    constructor(props){
        super(props);
        this.state = {
            creditCards: []
        }
        this.deleteRow = this.deleteRow.bind(this);
    }

    componentWillMount(){
        let _this = this;
        Axios.get('http://localhost:3001/creditCards/cards/' + sessionStorage.getItem('usuario'))
        .then(function (response){
            _this.setState({
                creditCards: response.data.data
            })
        }).catch(function (error){
            console.log(error);
        })
    }

    editRow = (card) => {
        this.props.history.push('/saveCreditCard/' + card.id);
    }

    deleteRow = (card) => {
        
        Axios.delete('http://localhost:3001/creditCards/' + card.id)
        .then(function(response){
            alert('Credit Card succesfully deleted');
        }).catch(function (error){
            console.log(error);
        })

        window.location.reload();

    }

    render(){
        let creditCards = this.state.creditCards;
        
        return(
            <div>
                <Menu />
                <Table striped bordered condensed hover responsive>
                    <thead>
                        <tr>
                            <th> Number </th>
                            <th> Security Code </th>
                            <th> Name </th>
                            <th> Expiry Date Month </th>
                            <th> Expiry Date Year </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                        {creditCards.map(
                            creditCard => 
                                <tr>
                                    <td> {creditCard.number} </td>
                                    <td> {creditCard.securityCode} </td>
                                    <td> {creditCard.name} </td>
                                    <td> {creditCard.expiryDateMonth} </td>
                                    <td> {creditCard.expiryDateYear} </td>
                                    <td> 
                                        <Button onClick={this.editRow.bind(this, creditCard)}> <Glyphicon glyph="edit" /> </Button>
                                        <Button onClick={this.deleteRow.bind(this, creditCard)}> <Glyphicon glyph="remove" /> </Button> 
                                    </td>
                                </tr>
                        )}
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default CreditCards;