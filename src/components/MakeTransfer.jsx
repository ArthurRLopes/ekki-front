import React, { Component } from 'react'
import Menu from './Menu.jsx';
import Axios from 'axios';
import TransferAlert from './TransferAlert.jsx'
import { FormGroup, ControlLabel, FormControl, InputGroup, InputGroupAddon, Button } from 'react-bootstrap';
import Dialog from 'react-bootstrap-dialog'


class MakeTransfer extends Component {
    constructor(props){
        super(props);
        this.state = {
            users: [],
            idAccountTo: '',
            amount: '',
            errorMessage: '',
            showAlert: false,
            alertWithButton: false,
            email: ''
        }

        this.checkLoginAndSendTransfer = this.checkLoginAndSendTransfer.bind(this);
    }

    async checkLoginAndSendTransfer(){
        const _this = this;
        let signedIn = false;
        let email = '';
        if (_this.state.amount > 1000){
            this.dialog.show({
                title: 'Please, enter your password',
                prompt: Dialog.PasswordPrompt(),
                actions: [
                    Dialog.CancelAction(),
                    Dialog.OKAction((dialog) => {
                        let password = dialog.value;
                        if (password != ''){
                            Axios.get('http://localhost:3001/users/get/' + sessionStorage.getItem("usuario"))
                            .then(function (response){
                                email = response.data.data.email;
                            });
                            
                            setTimeout(() => {
                                Axios.get('http://localhost:3001/users/auth', {
                                    params: {
                                        email: email,
                                        password: password
                                    }
                                }).then(function(response){
                                    
                                    if (response.data.success === true){
                                        
                                        _this.sendTransfer();
                                        
                                    } else {
                                        alert('Wrong Password!');
                                    }
                                });
                                dialog.hide();
                            }, 1000);
                        }
                    })
                ]
            });
        } else {
            _this.sendTransfer();
        }

        return signedIn;
    }

    sendTransfer = () => {
        const _this = this;
        Axios.post('http://localhost:3001/transactions/', {
            params: {
                idAccountFrom: sessionStorage.getItem("usuario"),
                idAccountTo: _this.state.idAccountTo,
                amount: _this.state.amount
            }
        }).then(function(response){
            if (response.data.success && typeof response.data.result.status === "undefined"){
                if (response.data.result.command == "UPDATE"){
                    alert('You did the same transfer less than 2 minutes ago. It will be overwritten');
                } else {
                    alert('Transfer success');
                }
            } else {
                _this.setState({
                    showAlert: true,
                    alertWithButton: response.data.result.status == 'no-funds-with-credit-card' 
                    ? false
                    : true,
                    errorMessage: response.data.result.status == 'no-funds-with-credit-card' 
                    ? 'You do not have funds to do this operation, it will be debited from your credit card' 
                    : 'You do not have funds to do this operation, and have no registred credit card. Would you like to register one now?' 
                })
            }
        }).catch(function(error){
            console.log(error);
        })
    }

    componentWillMount(){
        let _this = this;
        Axios.get('http://localhost:3001/users/', {
            params: {
                id: sessionStorage.getItem("usuario")
            }
        })
        .then(function(response){
            _this.setState({
                users: response.data.data,
                idAccountTo: response.data.data[0].id
            });
        }).catch(function(error){
            console.log(error);
        })
    }

    handleSubmit = event => {
        event.preventDefault();
        //let _this = this;
        //let signedIn;

        this.checkLoginAndSendTransfer();
        
        // if (_this.state.amount > 1000){
        //     _this.checkLogin().then((response) => {
        //         signedIn = response;

        //         if (signedIn === true){
        //             Axios.post('http://localhost:3001/transactions/', {
        //                 params: {
        //                     idAccountFrom: sessionStorage.getItem("usuario"),
        //                     idAccountTo: _this.state.idAccountTo,
        //                     amount: _this.state.amount
        //                 }
        //             }).then(function(response){
                        
        //                 if (response.data.success && typeof response.status === "undefined"){
        //                     alert('Transfer success');
        //                 } else {
        //                     _this.setState({
        //                         showAlert: true,
        //                         alertWithButton: response.data.result.status == 'no-funds-with-credit-card' 
        //                         ? false
        //                         : true,
        //                         errorMessage: response.data.result.status == 'no-funds-with-credit-card' 
        //                         ? 'You do not have funds to do this operation, it will be debited from your credit card' 
        //                         : 'You do not have funds to do this operation, and have no registred credit card. Would you like to register one now?' 
        //                     })
        //                 }
        //             }).catch(function(error){
        //                 console.log(error);
        //             })
        //         } else {
        //             alert('Wrong Password');
        //         }
        //     })
        // }

        
        

    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    render(){
        let users = this.state.users;
        return(
            <div>
                <Menu />
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="idAccountTo">
                        <ControlLabel> To Who? </ControlLabel>
                        <FormControl componentClass="select" onChange={this.handleChange}>
                            {users.map(
                                user => <option value={user.id}> {user.name} </option>
                            )}
                        </FormControl>
                    </FormGroup>
                    <FormGroup controlId="amount">
                        <ControlLabel> Amount? </ControlLabel>
                        <InputGroup>
                            <InputGroup.Addon> $ </InputGroup.Addon>
                            <FormControl type="number" step=".01" onChange={this.handleChange}/>
                        </InputGroup>
                    </FormGroup>
                    <Button type="submit">
                        Enviar
                    </Button>
                    <TransferAlert show={this.state.showAlert} withButton={this.state.alertWithButton} errorMessage={this.state.errorMessage} />
                    <Dialog ref={(el) => { this.dialog = el }} />
                </form>
            </div>
        );
    }
}

export default MakeTransfer;