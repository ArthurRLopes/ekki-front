import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel, Grid } from 'react-bootstrap';
import '../styles/login.css';
import Axios from "axios";

class Login extends Component{

    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: ''
        }
    }

    validateForm(){
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
          [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        let _this = this;
        Axios.get('http://localhost:3001/users/auth', {
            params: {
                email: this.state.email,
                password: this.state.password
            }
        })
        .then(function (response) {
            if (response.data.success !== false){
                _this.props.onLogin(response.data.data.id);
                sessionStorage.setItem("usuario", response.data.data.id);
                _this.props.history.push('/home');
            }
        }).catch(function (error){
            console.log("ERRO: " + error);
        });

        event.preventDefault();
    }

    render(){
        return (
            <Grid className="grid">
                <h1 className="cabecalho"> Bem-vindo ao banco Ekki! </h1>
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="email" bsSize="large">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                        autoFocus
                        type="email"
                        value={this.state.email}
                        onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                        value={this.state.password}
                        onChange={this.handleChange}
                        type="password"
                        />
                    </FormGroup>
                    <Button
                        block
                        bsSize="medium"
                        disabled={!this.validateForm()}
                        type="submit"
                    >
                        Login
                    </Button>
                    <Button block bsSize="medium" href="/createAccount"> Create Account </Button>
                </form>
            </Grid>
        );
    }
}

export default Login;