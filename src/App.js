import React, { Component } from 'react';
import './styles/App.css';
import Login from './components/Login.jsx';
import Home from './components/Home.jsx';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import MakeTransfer from './components/MakeTransfer.jsx';
import Transfers from './components/Transfers';
import CreditCard from './components/CreditCard';
import CreditCards from './components/CreditCards';
import Contact from './components/Contact';
import Contacts from './components/Contacts';
import CreateAccount from './components/CreateAccount';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      user: null
    }

    this.onLogin = this.onLogin.bind(this);
  }

  onLogin(user){
    this.setState({
      user: user
    })
  }

  render() {
    return (
      <Router>
        <Switch>
          <Route path="/login" render={(props)=>(
            <Login onLogin={this.onLogin} {...props}/>
          )}/>
          <Route path="/home" render={(props)=>(
            <Home user={this.state.user} {...props}/>
          )}/>
          <Route path="/makeTransfer" component={MakeTransfer} />
          <Route path="/transfers" component={Transfers} />
          <Route path="/saveCreditCard/:id" component={CreditCard} />
          <Route path="/saveCreditCard" component={CreditCard} />
          <Route path="/listCreditCards" component={CreditCards} />
          <Route path="/saveContact/:id" component={Contact} />
          <Route path="/saveContact" component={Contact} />
          <Route path="/listContacts/" component={Contacts} />
          <Route path="/createAccount" component={CreateAccount} />
          <Route path="/" render={(props)=>(
            <Login onLogin={this.onLogin} {...props}/>
          )}/>
        </Switch>
      </Router>
    );
  }
}

export default App;
